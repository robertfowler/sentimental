import datetime
import logging

import sqlalchemy

logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.INFO)


def mkrow(table, rec):
    drow_dict = dict((key.lower(), value) for key, value in rec.items())
    for col in table.columns:
        column_name = col.name
        if column_name in drow_dict:
            value = drow_dict[column_name]
            try:
                if value is None:
                    yield 'Null'
                elif isinstance(col.type, sqlalchemy.sql.sqltypes.String):
                    if col.type.length and len(value) > col.type.length:
                        logger.warning("Column '%s' too long at length %d for defined type length %d. Truncating." %
                                       (col.name, len(value), col.type.length))
                        value = value[:col.type.length]
                    # if the value is not a string (could be float, int, etc) the type is not correct in the database but we format
                    # it sensibly anyway and continue with a warning
                    if isinstance(value, datetime.datetime):
                        yield value.strftime('%Y-%m-%d %H:%M:%S')
                    elif isinstance(value, int) or isinstance(value, float):
                        yield str(value)
                    elif not isinstance(value, str) and not isinstance(value, unicode):
                        logger.warning("type wrong for column name '%s' python type '%s' sql type '%s'" % (column_name, type(value), col.type))
                        yield '"' + str(value).replace('"', '""') + '"'
                    else:
                        yield '"' + value.replace('"', '""') + '"'
                elif isinstance(col.type, sqlalchemy.sql.sqltypes.DateTime):
                    yield value.strftime('%Y-%m-%d %H:%M:%S')
                else:
                    yield str(value)
            except TypeError as ee:
                logger.error("type error: dropping into IPython: name '%s' type '%s' error '%s'" % (column_name, col.type, str(ee)))
                from IPython import embed
                embed()
        else:
            yield 'Null'


def sqlcsvwrite(ofile, table, results):
    ofile.write(','.join(map(lambda aa: aa.lower(), table.columns.keys())) + '\n')
    records = 0
    for rec in results:
        ofile.write(','.join(mkrow(table, rec._asdict())) + '\n')
        records += 1
    return records
