import sys, os
import optparse
import logging
import gzip

#from pattern.en import sentiment
from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import sessionmaker

sys.path.append('..')
import bicommon

from sqlacsv2 import sqlcsvwrite

logger = logging.getLogger('sentimental')
logging.basicConfig()
logger.setLevel(logging.INFO)

bicl = logging.getLogger('bicommon')
bicl.setLevel(logging.INFO)

DEFAULT_PERSONALITY = 'sentimental'

if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option("-p", "--personality", dest="personality", default=DEFAULT_PERSONALITY, help="personality. (defaults.ini file section)")
    parser.add_option("-j", "--job-name", dest="job_name", default="zensent", help="use this as the job name")
    parser.add_option("-C", "--create-tables", dest="create_tables", action="store_true", help="Create tables in database", default=None)
    parser.add_option("-c", "--config", dest="config_file", help="use config file", default="defaults.ini")
    parser.add_option("-N", "--config-section", dest="config_section", help="config file section", default="sentimental")
    parser.add_option("-v", "--dump-csv-file", dest="dump_csv_file", default=None, help="dump tickets table to csv")
    parser.add_option("-O", "--copy-tables", dest="copy_tables",
                      action="store_true", help="Copy data from zendesk to postgres using SQL (no good for redshift)", default=None)
    parser.add_option("-g", "--gzip", action="store_true", dest="gzip", default=False, help="Gzip the csv files when writing")
    parser.add_option("-u", "--no-upload", dest="no_upload", action="store_true", default=None,
                      help="Don't upload to S3 (good for local postgres in test")
    parser.add_option("-n", "--no-download", dest="no_download", action="store_true", default=None, help="Don't download the csv from source database")
    parser.add_option("-o", "--no-copy-in", dest="no_copy_in", action="store_true", default=None, help="Don't copy into the database")
    parser.add_option("-t", "--truncate", action="store_true", dest="truncate", default=False, help="Truncate before insert")
    parser.add_option("-X", "--exit-on-error", action="store_true", dest="exit_on_error", default=False, help="Exit if copy in has an error")

    cmd_line_options, args = parser.parse_args()
    config_file_options = bicommon.FFXConfig(config_file=cmd_line_options.config_file)

    # insert the config dictionaries into the magic options class
    options = bicommon.MagicOptions(os.environ,
                                    config_file_options.section_as_dict(cmd_line_options.personality),
                                    vars(cmd_line_options))

    if options.ENVIRONMENT:
        logger.warning("using an environment from config file to override real environment")
        options.update(config_file_options.section_as_dict("environment_%s" % options.ENVIRONMENT))

    zen_engine = create_engine(options.zen_uri)
    red_engine = create_engine(options.red_uri)
    zen_schema = options.zen_schema
    red_schema = options.schemaTableStaging
    table_name = options.tickets_table
    meta = MetaData()

    zen_tickets = Table(table_name, meta, autoload=True, autoload_with=zen_engine, schema=zen_schema)
    if options.create_tables:
        # to create the table shape that is in zendesk inside redshift
        # use the reflected table from zen, change the 'schema' in the reflected metadata to the correct one for redshift
        # and create the table in redhshift
        zen_tickets.schema = red_schema  # this sets the zen schema to red
        zen_tickets.create(red_engine, checkfirst=True)
        zen_tickets.schema = zen_schema

    red_tickets = Table(table_name, meta, autoload=True, autoload_with=red_engine, schema=red_schema)

    ZenSessionMaker = sessionmaker(bind=zen_engine)
    zen_session = ZenSessionMaker()
    RedSessionMaker = sessionmaker(bind=red_engine)
    red_session = RedSessionMaker()
    zen_tickets = Table(table_name, meta, autoload=True, autoload_with=zen_engine, schema=zen_schema)

    if options.copy_tables:
        results = zen_session.query(zen_tickets)
        for ii, result in enumerate(results.all()):
            red_session.execute(red_tickets.insert(result._asdict()))
            if not (ii % 50):
                print "done", ii
        red_session.commit()

    jmgr = bicommon.JobManager.from_options(red_session, meta, options)

    with jmgr.make_job(options.job_name) as job:
        if options.dump_csv_file and not options.no_download:
            with job.sub_job(sub_process_name='query zendesk for tickets', sub_batch_code='query') as sub_job:
                results = zen_session.query(zen_tickets).all()
                sub_job.count = len(results)
            with job.sub_job(sub_process_name='write tickets to csv', sub_batch_code='write csv') as sub_job:
                opener = gzip.open if options.gzip else open
                with opener(options.dump_csv_file, 'w') as fp:
                    sub_job.count = sqlcsvwrite(fp, zen_tickets, results)

        if not options.no_upload:
            with job.sub_job(sub_process_name='S3Upload', sub_batch_code='S3Upload') as sub_job:
                s3mgr = bicommon.S3Mgr(config_file_options, aws_bucket=options.aws_bucket, gzipped=options.gzip, bucket_subdir=options.bucket_subdir)
                # Special case, not passing options here because the AWS login is in the [AWS] section so boto should work without change
                s3mgr.upload_file(options.dump_csv_file, red_tickets.name)

        if not options.no_copy_in:
            with job.sub_job(sub_process_name='copy_in', sub_batch_code='copying in') as sub_job:
                # copy in manager expects 'options.uri' so red is the target to copy into so set it here
                options.update({'uri': options.red_uri, 'schema': red_schema})
                logger.info("Copy in")
                cpmgr = bicommon.CopyInMgr.from_options(options, config_file_options, red_session, meta)
                cpmgr.copy_in(red_tickets.name)
