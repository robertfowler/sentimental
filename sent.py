#!/usr/bin/env python
import optparse
import glob
import ConfigParser

from pattern.en import sentiment
from sqlalchemy import create_engine, Table
from sqlalchemy.orm import sessionmaker

from schema import Base, Source, Sentiment

#
# select source.attribute, sentiment.polarity,sentiment.subjectivity,
#  substring(source.text for 80) from sentiment, source where source.id = sentiment.source_pkey;


# extract the source table by reversing the database and using column names
#
def update_sentiments(session, metadata, sdesc, attribute):
    tbl = Table(sdesc['table'], metadata, autoload=True)  # schema='schemaname')
    dta = session.query(tbl.columns[sdesc['pkey']],
                        tbl.columns[sdesc['text']])
    if attribute != 'all':
        dta = dta.filter(tbl.c.attribute == attribute)  # cheating here. This is only for the test data table where I know the attribute
    for ii in dta.all():
        polarity, subjectivity = sentiment(ii[1])
        ss = Sentiment(source_id=sdesc['name'], source_pkey=ii[0], polarity=polarity, subjectivity=subjectivity)
        session.merge(ss)
    session.commit()

if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option("-C", "--create-tables", dest="create_tables", action="store_true", help="Create tables in database", default=None)
    parser.add_option("-c", "--config", dest="config_file", help="use config file", default="defaults.ini")
    parser.add_option("-l", "--load", dest="load", help="load data", default=None)
    parser.add_option("-a", "--attribute", dest="attribute", help="Add to attribute column", default=None)
    parser.add_option("-t", "--stest", dest="stest", help="Run test on this attribute or all", default=None)
    options, args = parser.parse_args()

    config = ConfigParser.ConfigParser()
    config.readfp(open(options.config_file))

    engine = create_engine(config.get('db', 'uri'))
    MakeSession = sessionmaker(bind=engine)

    if options.create_tables:
        print "creating all tables"
        Base.metadata.create_all(engine)
    if options.load:
        session = MakeSession()
        for fname in glob.glob(options.load):
            with open(fname) as fp:
                st = Source(text=fp.read(), attribute=options.attribute)
                session.add(st)
        session.commit()
        session.close()
    if options.stest:
        # make a new session
        sdesc = {'name': 'testdata',
                 'table': 'source',
                 'pkey': 'id',
                 'text': 'text'}
        update_sentiments(MakeSession(), Base.metadata, sdesc, options.stest)
