### What is this repository for? ###

* This contains the basic framework to connect to a database and to generate sentiment polarity and subjectivity factors which are then stored into a new table.

* Version: 0.1 Just a proof of concept.

### How do I get set up? ###

* Set up
git clone https://USERNAME@bitbucket.org/robertfowler/sentimental.git (See above right)
* Configuration is read from the local defaults.ini:
* Database configuration:

     [db]

     uri=postgresql+psycopg2:///sent

See http://docs.sqlalchemy.org/en/rel_0_9/core/engines.html#oracle for connection to oracle, mysql and postgres.

* Dependencies
create a virtualenv, activate it then:
     pip install -r requirements.txt

## How to run tests ##
Pre create the schema with:
    ./sent.py --create-tables
Creates: 'sentiment' the target table with sentiments and 'source' for the example/test data.

To load test data, get review_polarity.tar.gz and untar it into your directory yielding txt_sentoken/
The load negatives with:
     ./sent.py -l txt_sentoken/neg/\* -a neg
then the positives:
     ./sent.py -l txt_sentoken/pos/\* -a pos

-a specifies an additional attribute for the test data so you can select rows based on known positive or negative sentiments.

Then run the:
    ./sent.py -t all
to generate and populate sentiments.

You can then run:
select source.attribute, sentiment.polarity,sentiment.subjectivity,
 substring(source.text for 80) from sentiment, source where source.id = sentiment.source_pkey;

## Sourcing from other tables ##

The code to store the sentiments uses the following json block to describe where the sentiment data should go:

     sdesc = {'name': 'testdata',
                 'table': 'source',
                 'pkey': 'id',
                 'text': 'text'}

- name is the name of the source system. It is used as a composite component of the key id in the sentiment data table
- table is the source table
- pkey is the source table key that is used as a composite key for the target data table
- text is the field that contains the text

### References ###

- The paper on the sentiment analysis: http://www.clips.ua.ac.be/sites/default/files/desmedt-subjectivity.pdf
- The package: http://www.clips.ua.ac.be/pages/pattern