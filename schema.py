from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, ForeignKey
from sqlalchemy import types


Base = declarative_base()


class Source(Base):
    __tablename__ = 'source'
    id = Column(types.Integer, primary_key=True)
    text = Column(types.String)
    attribute = Column(types.String)


class Sentiment(Base):
    __tablename__ = 'sentiment'
    source_pkey = Column(types.BigInteger, primary_key=True)       # foreign  key to something somewhere
    source_id = Column(types.String(30), primary_key=True)       # foreign  key to something somewhere
    polarity = Column(types.Float)
    subjectivity = Column(types.Float)
